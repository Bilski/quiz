package com.example.jonasz.quiz

interface AnswerRowView {

    fun setAnswer(answer: String)
}