package com.example.jonasz.quiz

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object Constant {

    const val PARAM_QUIZ_BUNDLE = "quizBundle"
    const val PARAM_QUIZ_RESULT = "quizResult"
    const val BASE_URL = "http://quiz.o2.pl/"
}