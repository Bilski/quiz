package com.example.jonasz.quiz

internal interface ListingRowView {

    fun setTitle(title: String)

    fun setResult(result: String)

    fun setThumbnail(url: String?)

    fun hideResult()
}