package com.example.jonasz.quiz.activities

import io.appflate.droidmvp.DroidMVPActivity
import io.appflate.droidmvp.DroidMVPPresenter
import io.appflate.droidmvp.DroidMVPView

abstract class BaseActivity<M, V : DroidMVPView, P : DroidMVPPresenter<V, M>> : DroidMVPActivity<M, V, P>() {

    override fun performFieldInjection() {
        //since we don't use any dependency injection framework here, lets make this method no-op.
    }
}