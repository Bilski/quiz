package com.example.jonasz.quiz.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.jonasz.quiz.Constant
import com.example.jonasz.quiz.R
import com.example.jonasz.quiz.adapters.ListingRecyclerAdapter
import com.example.jonasz.quiz.dataLayer.rdp.RetrofitQuizRepository
import com.example.jonasz.quiz.model.ListingPresentationModel
import com.example.jonasz.quiz.model.bundle.QuizBundle
import com.example.jonasz.quiz.mvpviews.ListingView
import com.example.jonasz.quiz.presenters.ListingPresenter
import kotlinx.android.synthetic.main.activity_main.*

class ListingActivity : BaseActivity<ListingPresentationModel, ListingView, ListingPresenter>(), ListingView {

    override fun createPresentationModel(): ListingPresentationModel {
        return ListingPresentationModel()
    }

    override fun createPresenter(): ListingPresenter {

        val remoteRepository = RetrofitQuizRepository()

        return ListingPresenter(remoteRepository)
    }

    override fun showQuizzes() {
        listingView.adapter.notifyDataSetChanged()
    }

    override fun showQuizDetailScreen(quiz: QuizBundle) {
        val intent = Intent(this, QuizDetailActivity::class.java)
        intent.putExtra(Constant.PARAM_QUIZ_BUNDLE, quiz)
        startActivity(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        listingView.layoutManager = LinearLayoutManager(this)
        listingView.adapter = ListingRecyclerAdapter(presenter)
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.unbind()
    }
}
