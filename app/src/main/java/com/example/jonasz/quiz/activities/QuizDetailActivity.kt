package com.example.jonasz.quiz.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.bumptech.glide.Glide
import com.example.jonasz.quiz.Constant
import com.example.jonasz.quiz.R
import com.example.jonasz.quiz.adapters.QuizDetailRecyclerAdapter
import com.example.jonasz.quiz.createRetrofitInstance
import com.example.jonasz.quiz.dataLayer.rdp.RetrofitQuizDetailRepository
import com.example.jonasz.quiz.model.QuizDetailPresentationModel
import com.example.jonasz.quiz.model.bundle.QuizBundle
import com.example.jonasz.quiz.mvpviews.QuizDetailView
import com.example.jonasz.quiz.presenters.QuizDetailPresenter
import kotlinx.android.synthetic.main.quiz_detail.*

class QuizDetailActivity : BaseActivity<QuizDetailPresentationModel, QuizDetailView, QuizDetailPresenter>(), QuizDetailView {

    override fun createPresentationModel(): QuizDetailPresentationModel {
        val quiz = intent.getSerializableExtra(Constant.PARAM_QUIZ_BUNDLE) as QuizBundle
        return QuizDetailPresentationModel(quiz = quiz)
    }

    override fun createPresenter(): QuizDetailPresenter {
        val repository = RetrofitQuizDetailRepository()

        return QuizDetailPresenter(repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.quiz_detail)
        answersView.layoutManager = LinearLayoutManager(this)
        answersView.adapter = QuizDetailRecyclerAdapter(presenter)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.unbind()
    }

    override fun showAnswers() {
        answersView.adapter.notifyDataSetChanged()
    }

    override fun showQuizTitle(text: String) {
        quizTitle.text = text
    }

    override fun showQuestion(text: String) {
        questionTitle.text = text
    }

    override fun updateProgress(progress: Int) {
        if (Build.VERSION.SDK_INT >= 24) {
            progressBar.setProgress(progress, true)
        } else {
            progressBar.visibility = View.GONE
        }
    }

    override fun setImage(url: String?) {
        Glide.with(this.quizImage.context)
            .load(url)
            .into(quizImage)
    }

    override fun hideImage() {
        quizImage.visibility = View.GONE
    }

    override fun showImage() {
        quizImage.visibility = View.VISIBLE
    }

    override fun setMaxNumberOnProgressBar(maxNumber: Int) {
        progressBar.max = maxNumber
    }

    override fun showResultScreen(result: String) {
        val intent = Intent(this, ResultActivity::class.java)
        intent.putExtra(Constant.PARAM_QUIZ_RESULT, result)
        startActivity(intent)
    }

    override fun onBackPressed() {
        presenter.onBackButtonClicked()
    }

    override fun close() {
        finish()
    }
}