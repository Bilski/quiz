package com.example.jonasz.quiz.activities

import android.os.Bundle
import com.example.jonasz.quiz.Constant
import com.example.jonasz.quiz.R
import com.example.jonasz.quiz.model.ResultPresentationModel
import com.example.jonasz.quiz.mvpviews.ResultView
import com.example.jonasz.quiz.presenters.ResultPresenter
import io.appflate.droidmvp.SimpleDroidMVPPresenter
import kotlinx.android.synthetic.main.result_view.*
import javax.xml.transform.Result

class ResultActivity() : BaseActivity<ResultPresentationModel, ResultView, ResultPresenter>(), ResultView {

    override fun createPresentationModel(): ResultPresentationModel {
        val result = intent.getStringExtra(Constant.PARAM_QUIZ_RESULT)
        return ResultPresentationModel(result)
    }

    override fun createPresenter(): ResultPresenter {
        return ResultPresenter()
    }

    override fun showResult(result: String) {
        resultText.text = result
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.result_view)
    }

    override fun onBackPressed() {} //blokada powrotu przez "Back"
}