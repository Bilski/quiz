package com.example.jonasz.quiz.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.jonasz.quiz.ListingRowView
import com.example.jonasz.quiz.R
import com.example.jonasz.quiz.extensions.inflate
import com.example.jonasz.quiz.presenters.ListingPresenter
import kotlinx.android.synthetic.main.recycler_item_view.view.*

class ListingRecyclerAdapter(private val presenter: ListingPresenter) : RecyclerView.Adapter<ListingRecyclerAdapter.QuizHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuizHolder {
        val inflatedView = parent.inflate(R.layout.recycler_item_view, false)
        return ListingRecyclerAdapter.QuizHolder(inflatedView)
    }

    override fun getItemCount(): Int {
        return presenter.getNumberOfQuizzes()
    }

    override fun onBindViewHolder(holder: QuizHolder, position: Int) {
        presenter.onBindQuizRowViewAtPosition(position, holder)
        holder.itemView.setOnClickListener {
            presenter.onItemClick(position)
        }
    }

    class QuizHolder(v: View) : RecyclerView.ViewHolder(v),
        ListingRowView {

        private var view: View = v

        override fun setTitle(title: String) {
            view.quizTitle.text = title
        }

        override fun setResult(result: String) {
            view.quizResult.text = result
        }

        override fun setThumbnail(url: String?) {
            Glide.with(this.view.context)
                .load(url)
                .into(view.quizThumbnail)
        }

        override fun hideResult() {
            view.quizResult.visibility = View.GONE
        }
    }
}