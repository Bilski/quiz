package com.example.jonasz.quiz.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.example.jonasz.quiz.AnswerRowView
import com.example.jonasz.quiz.R
import com.example.jonasz.quiz.extensions.inflate
import com.example.jonasz.quiz.presenters.QuizDetailPresenter
import kotlinx.android.synthetic.main.answer_item_view.view.*

class QuizDetailRecyclerAdapter(private var presenter: QuizDetailPresenter) : RecyclerView.Adapter<QuizDetailRecyclerAdapter.AnswerHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnswerHolder {
        val inflatedView = parent.inflate(R.layout.answer_item_view, false)
        return QuizDetailRecyclerAdapter.AnswerHolder(inflatedView)
    }

    override fun getItemCount(): Int {
        return presenter.getNumberOfCurrentAnswers()
    }

    override fun onBindViewHolder(holder: AnswerHolder, position: Int) {
        presenter.onBindAnswerRowViewAtPosition(position, holder)
        holder.itemView.setOnClickListener {
            presenter.onItemClick(position)
        }
    }

    class AnswerHolder(v: View) : RecyclerView.ViewHolder(v),
        AnswerRowView {

        private var view: View = v

        override fun setAnswer(answer: String) {
            view.answerText.text = answer
        }
    }
}