package com.example.jonasz.quiz.dataLayer.endpoint

interface Endpoint {

    fun getListingEndpoint(): String

    fun getQuizEndpoint(id: Long): String
}