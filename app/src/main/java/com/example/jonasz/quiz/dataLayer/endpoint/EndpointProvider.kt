package com.example.jonasz.quiz.dataLayer.endpoint

object EndpointProvider: Endpoint {

    private const val LISTING_ENDPOINT = "api/v1/quizzes/0/100"
    private const val QUIZ_ENDPOINT = "api/v1/quiz/{id}/0"

    override fun getListingEndpoint(): String =
        LISTING_ENDPOINT

    override fun getQuizEndpoint(id: Long): String = QUIZ_ENDPOINT.replace("{id}", id.toString())
}