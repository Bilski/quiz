package com.example.jonasz.quiz.dataLayer.entity

import java.io.Serializable

data class Answer(
    val order: Int,
    val isCorrect: Boolean,
    val text: String
) : Serializable