package com.example.jonasz.quiz.dataLayer.entity

data class Listing(
    val count: Int,
    val quizzes: List<Quiz>
)