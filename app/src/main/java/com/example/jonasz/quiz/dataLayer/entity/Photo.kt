package com.example.jonasz.quiz.dataLayer.entity

import java.io.Serializable

data class Photo(
    val author: String,
    val source: String,
    val title: String,
    val url: String
) : Serializable