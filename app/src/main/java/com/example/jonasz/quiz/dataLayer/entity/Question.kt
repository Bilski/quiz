package com.example.jonasz.quiz.dataLayer.entity

import com.example.jonasz.quiz.dataLayer.entity.Photo

import java.io.Serializable

data class Question(
    val order: Int,
    val type: String,
    val answer: String,
    val photo: Photo?,
    val text: String,
    val answers: List<Answer>
) : Serializable