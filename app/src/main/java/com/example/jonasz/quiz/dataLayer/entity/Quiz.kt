package com.example.jonasz.quiz.dataLayer.entity

import java.io.Serializable

data class Quiz(
    val content: String,
    val id: Long,
    val photo: Photo?,
    val numberOfCorrectAnswers: Int,
    val questionIndex: Int,
    val questions: Int,
    val finished: Boolean,
    val sponsored: Boolean,
    val title: String,
    val type: String
) : Serializable