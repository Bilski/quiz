package com.example.jonasz.quiz.dataLayer.entity

import java.io.Serializable

data class QuizDetail(
    val id: Long,
    val title: String,
    val questions: List<Question>
) : Serializable