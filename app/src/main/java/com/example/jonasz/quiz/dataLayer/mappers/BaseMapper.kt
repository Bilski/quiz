package com.example.jonasz.quiz.dataLayer.mappers

abstract class BaseMapper<in From : Any, out To> : Mapper<From, To> {

    override fun mapOrNull(from: From): To? {
        throw NotImplementedError()
    }

    override fun mapOrSkip(from: From): To? =
        mapOrNull(from)

    override fun mapOrSkip(from: List<From?>): List<To> =
        map(from) { it, to -> mapAndAppendOrSkip(it, to) }

    @Suppress("USELESS_CAST")
    private fun map(from: Collection<From?>, mapAndAppendTo: (From, MutableList<To>) -> Unit) =
        with(mutableListOf<To>()) {
            from.filterNotNull().forEach { mapAndAppendTo.invoke(it, this) }
            return@with this
        } as List<To>


    private fun mapAndAppendOrSkip(it: From, to: MutableList<To>) {
        mapOrNull(it)?.let { to.add(it) }
    }
}