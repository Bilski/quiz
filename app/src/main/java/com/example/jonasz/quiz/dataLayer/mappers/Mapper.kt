package com.example.jonasz.quiz.dataLayer.mappers

internal interface Mapper<in From, out To> {

    fun mapOrNull(from: From): To?

    fun mapOrSkip(from: From): To?

    fun mapOrSkip(from: List<From?>): List<To>

}