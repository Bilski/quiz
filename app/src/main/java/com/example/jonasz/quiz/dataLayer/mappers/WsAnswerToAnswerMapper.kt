package com.example.jonasz.quiz.dataLayer.mappers

import com.example.jonasz.quiz.dataLayer.entity.Answer
import com.example.jonasz.quiz.dataLayer.model.WsAnswer

internal class WsAnswerToAnswerMapper : BaseMapper<WsAnswer, Answer>() {

    override fun mapOrNull(from: WsAnswer) =
            from.takeIf { it.isValid() }
                ?.let {
                    Answer(from.order!!,
                        from.mapIsCorrectProperty(),
                        from.text!!
                        )
                }

    private fun WsAnswer.mapIsCorrectProperty() =
        isCorrect != null && isCorrect == 1


    private fun WsAnswer.isValid() =
        order != null && text != null
}


