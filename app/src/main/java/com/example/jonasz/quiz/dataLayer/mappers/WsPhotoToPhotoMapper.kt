package com.example.jonasz.quiz.dataLayer.mappers

import com.example.jonasz.quiz.dataLayer.entity.Photo
import com.example.jonasz.quiz.dataLayer.model.WsPhoto

class WsPhotoToPhotoMapper : BaseMapper<WsPhoto, Photo>() {

    override fun mapOrNull(from: WsPhoto) =
            from.takeIf { it.isValid() }
                ?.let {
                    Photo(author = from.author ?: "",
                        source = from.source ?: "",
                        title = from.title ?: "",
                        url = from.url!!)
                }

    private fun WsPhoto.isValid(): Boolean =
            !this.isInvalid()

    private fun WsPhoto.isInvalid(): Boolean =
            url.isNullOrBlank()
}