package com.example.jonasz.quiz.dataLayer.mappers

import com.example.jonasz.quiz.dataLayer.entity.Photo
import com.example.jonasz.quiz.dataLayer.model.WsPhoto
import com.example.jonasz.quiz.dataLayer.entity.Question
import com.example.jonasz.quiz.dataLayer.model.WsQuestion

internal class WsQuestionToQuestionMapper : BaseMapper<WsQuestion, Question>() {

    private val wsAnswerToAnswerMapper = WsAnswerToAnswerMapper()
    private val wsPhotoToPhotoMapper = WsPhotoToPhotoMapper()

    override fun mapOrNull(from: WsQuestion): Question? =
        from.takeIf { it.isValid() }
            ?.let {
                Question(from.order!!,
                    from.type ?: "",
                    from.answer ?: "",
                    mapPhotoOrNull(from.photo),
                    from.text!!,
                    from.answers!!.mapNotNull {
                        wsAnswerToAnswerMapper.mapOrNull(it!!)
                    })
            }

    private fun WsQuestion.isValid() =
        order != null && text != null && answers != null

    private fun mapPhotoOrNull(photo: WsPhoto?): Photo? {
        photo?.let {
            return wsPhotoToPhotoMapper.mapOrNull(photo)
        }
        return null
    }
}