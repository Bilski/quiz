package com.example.jonasz.quiz.dataLayer.mappers

import com.example.jonasz.quiz.dataLayer.entity.QuizDetail
import com.example.jonasz.quiz.dataLayer.model.WsQuizDetail

internal class WsQuizDetailToQuizDetailMapper : BaseMapper<WsQuizDetail, QuizDetail>() {

    private val wsQuestionToQuestionMapper = WsQuestionToQuestionMapper()

    override fun mapOrNull(from: WsQuizDetail): QuizDetail? =
        from.takeIf { it.isValid() }
            ?.let {
                QuizDetail(id = from.id!!,
                    title = from.title!!,
                    questions = from.questions!!.mapNotNull {
                        wsQuestionToQuestionMapper.mapOrNull(it!!)
                    }
                )
            }

    private fun WsQuizDetail.isValid() =
            id != null
                    && title != null
                    && questions != null
                    && !questions.isEmpty()
}