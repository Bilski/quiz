package com.example.jonasz.quiz.dataLayer.mappers

import com.example.jonasz.quiz.dataLayer.entity.Photo
import com.example.jonasz.quiz.dataLayer.entity.Quiz
import com.example.jonasz.quiz.dataLayer.model.WsPhoto
import com.example.jonasz.quiz.dataLayer.model.WsQuiz

class WsQuizToQuizMapper : BaseMapper<WsQuiz, Quiz>() {

    private val wsPhotoToPhotoMapper = WsPhotoToPhotoMapper()

    override fun mapOrNull(from: WsQuiz) =
        from.takeIf { it.isValid() }
            ?.let {
                Quiz(content = from.content ?: "",
                    id = from.id!!,
                    photo = mapPhotoOrNull(from.photo),
                    numberOfCorrectAnswers = 0,
                    questionIndex = 0,
                    questions = from.questions!!,
                    finished = false,
                    sponsored = from.sponsored ?: false,
                    title = from.title!!,
                    type = from.type ?: ""
                )
            }

    private fun WsQuiz.isValid(): Boolean =
        !this.isInvalid()

    private fun WsQuiz.isInvalid(): Boolean =
        id == null
                || title.isNullOrBlank()
                || questions == null

    private fun mapPhotoOrNull(photo: WsPhoto?): Photo? {
        photo?.let {
            return wsPhotoToPhotoMapper.mapOrNull(photo)
        }
        return null
    }
}