package com.example.jonasz.quiz.dataLayer.model

data class WsAnswer(

    val order: Int?,
    val isCorrect: Int?,
    val text: String?
)