package com.example.jonasz.quiz.dataLayer.model

import com.google.gson.annotations.SerializedName

data class WsListing(
    val count: Int?,
    @SerializedName("items") val quizzes: List<WsQuiz?>?
)