package com.example.jonasz.quiz.dataLayer.model

data class WsPhoto(
    val author: String?,
    val source: String?,
    val title: String?,
    val url: String?
)