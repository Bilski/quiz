package com.example.jonasz.quiz.dataLayer.model

import com.google.gson.annotations.SerializedName

data class WsQuestion(

    val order: Int?,
    val type: String?,
    val answer: String?,
    @SerializedName("image") val photo: WsPhoto?,
    val text: String?,
    val answers: List<WsAnswer?>?
)