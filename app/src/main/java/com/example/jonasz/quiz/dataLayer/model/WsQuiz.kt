package com.example.jonasz.quiz.dataLayer.model

import com.google.gson.annotations.SerializedName

data class WsQuiz(
    val content: String?,
    val id: Long?,
    @SerializedName("mainPhoto") val photo: WsPhoto?,
    val questions: Int?,
    val sponsored: Boolean?,
    val title: String?,
    val type: String?
)