package com.example.jonasz.quiz.dataLayer.model

data class WsQuizDetail(

    val id: Long?,
    val title: String?,
    val questions: List<WsQuestion?>?
)