package com.example.jonasz.quiz.dataLayer.rdp

import com.example.jonasz.quiz.dataLayer.endpoint.EndpointProvider
import com.example.jonasz.quiz.dataLayer.entity.QuizDetail
import com.example.jonasz.quiz.dataLayer.mappers.WsQuizDetailToQuizDetailMapper
import com.example.jonasz.quiz.dataLayer.model.WsQuizDetail
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Url

class AllQuizDetailRetrofitSpecification
    : RetrofitSpecification<QuizDetail> {

    private val wsQuizDetailToQuizDetailMapper = WsQuizDetailToQuizDetailMapper()

    var quizId: Long = 0

    fun withQuizId(id: Long): AllQuizDetailRetrofitSpecification {
        quizId = id
        return this
    }

    override fun getResults(retrofit: Retrofit): Single<List<QuizDetail>> {
        val url = EndpointProvider.getQuizEndpoint(quizId)

        return retrofit.create(QuizDetailRetrofitSpecificationApiCall::class.java)
            .getQuizDetail(url)
            .map { wsQuizDetailToQuizDetailMapper.mapOrSkip(listOf(it)) }
    }

    private interface QuizDetailRetrofitSpecificationApiCall {

        @GET
        fun getQuizDetail(@Url url: String): Single<WsQuizDetail>
    }
}