package com.example.jonasz.quiz.dataLayer.rdp

import com.example.jonasz.quiz.dataLayer.endpoint.EndpointProvider
import com.example.jonasz.quiz.dataLayer.entity.Quiz
import com.example.jonasz.quiz.dataLayer.mappers.WsQuizToQuizMapper
import com.example.jonasz.quiz.dataLayer.model.WsListing
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Url

internal class AllQuizzesRetrofitSpecification
    : RetrofitSpecification<Quiz>, AllQuizzesSpecification {

    private val wsQuizToQuizMapper = WsQuizToQuizMapper()

    override fun getResults(retrofit: Retrofit): Single<List<Quiz>> {
        val url = EndpointProvider.getListingEndpoint()

        return retrofit
            .create(ListingRetrofitSpecificationApiCall::class.java)
            .getListing(url)
            .map { wsQuizToQuizMapper.mapOrSkip(it.quizzes!!)}
    }

    private interface ListingRetrofitSpecificationApiCall {

        @GET
        fun getListing(@Url url: String): Single<WsListing>
    }
}