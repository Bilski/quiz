package com.example.jonasz.quiz.dataLayer.rdp

import com.example.jonasz.quiz.dataLayer.entity.Quiz

interface AllQuizzesSpecification: Specification<Quiz>