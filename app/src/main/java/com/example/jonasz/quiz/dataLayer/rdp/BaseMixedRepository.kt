package com.example.jonasz.quiz.dataLayer.rdp

import io.reactivex.Completable
import io.reactivex.Observable

internal abstract class BaseMixedRepository<T>(
        val localRepository: Repository<T>,
        val remoteRepository: Repository<T>)
    : Repository<T> {

    override fun query(specification: Specification<T>): Observable<List<T>> =
        (specification as MixedSpecification<T>).getResults(localRepository, remoteRepository)

    override fun add(item: T) = localRepository.add(item)

    override fun add(items: List<T>) = localRepository.add(items)

    override fun remove(specification: Specification<T>) = localRepository.remove(specification)
}