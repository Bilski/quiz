package com.example.jonasz.quiz.dataLayer.rdp

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

internal abstract class BaseMixedSpecification<T>(
        private val localSpecification: Specification<T>,
        private val remoteSpecification: Specification<T>)
    : MixedSpecification<T> {

    override fun getResults(localRepository: Repository<T>, remoteRepository: Repository<T>): Observable<List<T>> {

        val localStream =
            localRepository
                .query(localSpecification)

        val remoteStream =
            remoteRepository
                .query(remoteSpecification)
                .flatMapSingle { remoteResults ->
                    localRepository.remove(localSpecification)
                        .toSingleDefault(remoteResults)
                        .doOnError {  }
                }
                .flatMapSingle { remoteResults ->
                    localRepository.add(remoteResults)
                        .toSingleDefault(remoteResults)
                        .doOnError {  }
                }
                .flatMap { localRepository.query(localSpecification) }

        return ascendingHierarchyMerge(
            localStream,
            remoteStream)
            .subscribeOn(Schedulers.io())

    }

    private fun <T> ascendingHierarchyMerge(vararg inputSources: Observable<T>): Observable<T> {
        var lastEmissionId = 0
        val hierarchizedSources = mutableListOf<Observable<T>>()
        inputSources.forEachIndexed { index, observable ->
            hierarchizedSources.add(
                observable
                    .filter { lastEmissionId <= index }
                    .doOnNext { lastEmissionId = index })
        }
        return Observable.mergeDelayError(hierarchizedSources)
    }
}
