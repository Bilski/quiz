package com.example.jonasz.quiz.dataLayer.rdp

import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.Retrofit

internal abstract class BaseRetrofitRepository<T>(override val retrofit: Retrofit) : RetrofitRepository<T> {

    override fun add(item: T): Completable {
        throw NotImplementedError()
    }

    override fun remove(specification: Specification<T>): Completable {
        throw NotImplementedError()
    }

    override fun add(items: List<T>): Completable {
        throw NotImplementedError()
    }


    override fun query(specification: Specification<T>): Observable<List<T>> {
        throw NotImplementedError()
    }
}