package com.example.jonasz.quiz.dataLayer.rdp

import com.example.jonasz.quiz.dataLayer.entity.Listing

internal class MixedListingRepository(localRepository: Repository<Listing>,
                                      remoteRepository: Repository<Listing>)
    : BaseMixedRepository<Listing>(localRepository, remoteRepository)