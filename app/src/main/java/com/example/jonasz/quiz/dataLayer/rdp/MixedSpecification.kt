package com.example.jonasz.quiz.dataLayer.rdp

import io.reactivex.Observable

internal interface MixedSpecification<T> : Specification<T> {

    fun getResults(localRepository: Repository<T>,
                   remoteRepository: Repository<T>): Observable<List<T>>
}