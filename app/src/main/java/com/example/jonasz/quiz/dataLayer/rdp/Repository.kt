package com.example.jonasz.quiz.dataLayer.rdp

import io.reactivex.Completable
import io.reactivex.Observable

interface Repository<TYPE> {

    fun query(specification: Specification<TYPE>): Observable<List<TYPE>>

    fun add(item: TYPE): Completable

    fun add(items: List<TYPE>): Completable

    fun remove(specification: Specification<TYPE>): Completable
}