package com.example.jonasz.quiz.dataLayer.rdp

import com.example.jonasz.quiz.createRetrofitInstance
import com.example.jonasz.quiz.dataLayer.entity.QuizDetail
import io.reactivex.Observable
import retrofit2.Retrofit

internal class RetrofitQuizDetailRepository
    : BaseRetrofitRepository<QuizDetail>(createRetrofitInstance()) {
    
    override fun query(specification: Specification<QuizDetail>): Observable<List<QuizDetail>> =
        (specification as RetrofitSpecification<QuizDetail>)
            .getResults(retrofit)
            .toObservable()
}