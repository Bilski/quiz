package com.example.jonasz.quiz.dataLayer.rdp

import com.example.jonasz.quiz.createRetrofitInstance
import com.example.jonasz.quiz.dataLayer.entity.Quiz

internal class RetrofitQuizRepository :
    BaseRetrofitRepository<Quiz>(createRetrofitInstance()) {

    override fun query(specification: Specification<Quiz>) =
        (specification as RetrofitSpecification<Quiz>)
            .getResults(retrofit)
            .toObservable()
}