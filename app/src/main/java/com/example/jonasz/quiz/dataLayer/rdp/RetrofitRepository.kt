package com.example.jonasz.quiz.dataLayer.rdp

import retrofit2.Retrofit

internal interface RetrofitRepository<TYPE> : Repository<TYPE> {
    val retrofit: Retrofit
}