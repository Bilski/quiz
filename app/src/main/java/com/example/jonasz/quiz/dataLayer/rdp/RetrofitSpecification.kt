package com.example.jonasz.quiz.dataLayer.rdp

import io.reactivex.Single
import retrofit2.Retrofit

internal interface RetrofitSpecification<TYPE> : Specification<TYPE> {

    fun getResults(retrofit: Retrofit): Single<List<TYPE>>
}