package com.example.jonasz.quiz.model

import com.example.jonasz.quiz.dataLayer.entity.Quiz
import java.io.Serializable

class ListingPresentationModel : Serializable  {

    private var quizzes: List<Quiz> = emptyList()

    fun setQuizzes(quizzes: List<Quiz>) {
        this.quizzes = quizzes
    }

    fun getQuizzes() = quizzes

    fun getNumberOfQuizzes() = getQuizzes().size

    fun getQuiz(index: Int) = getQuizzes()[index]

    fun shouldFetchQuizzes() = getQuizzes().isEmpty()
}