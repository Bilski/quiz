package com.example.jonasz.quiz.model

import com.example.jonasz.quiz.R.id.quizTitle
import com.example.jonasz.quiz.dataLayer.entity.Answer
import com.example.jonasz.quiz.dataLayer.entity.Question
import com.example.jonasz.quiz.dataLayer.entity.QuizDetail
import com.example.jonasz.quiz.model.bundle.QuizBundle
import java.io.Serializable

class QuizDetailPresentationModel(private var quiz: QuizBundle) : Serializable {

    private var questions: List<Question> = emptyList()

    fun setQuestions(questions: List<Question>) {
        this.questions = questions
    }

    fun shouldFetchQuestions() = questions.isEmpty()

    fun getQuizId() = quiz.id

    fun setFinishedFlagWithNewState(state: Boolean) {
        quiz.finished = state
    }

    fun resetQuestionIndex() {
        quiz.questionIndex = 0
    }

    fun incrementCurrentQuestionIndex() = quiz.questionIndex++

    fun getCurrentAnswers() : List<Answer> = getCurrentQuestion()?.answers ?: emptyList()

    fun getCurrentQuestionIndex() = quiz.questionIndex

    fun shouldGoToResultScreen() = quiz.questionIndex == getNumberOfQuestions() - 1

    fun incrementCorrectAnswerIfNeeded(position: Int) {
        if (checkIfUserChoseCorrectAnswer(position)) {
            quiz.numberOfCorrectAnswers++
        }
    }

    fun getNumberOfQuestions() = questions.size

    fun getNumberOfCorrectAnswers() = quiz.numberOfCorrectAnswers

    fun getQuestionPhotoUrl() : String? {
        return getCurrentQuestion()?.photo?.url
    }

    fun getQuizTitle() = quiz.title

    fun getCurrentQuestionTitle() = getCurrentQuestion()?.text ?: ""

    private fun getCurrentQuestion() : Question? {
        if (questions.isEmpty()) { return null }
        return questions[getCurrentQuestionIndex()]
    }

    private fun checkIfUserChoseCorrectAnswer(position: Int) : Boolean {
        val currentAnswers = getCurrentAnswers()
        return currentAnswers[position].isCorrect
    }
}