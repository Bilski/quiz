package com.example.jonasz.quiz.model

import java.io.Serializable

class ResultPresentationModel(private val result: String) : Serializable {

    fun getResult() = result
}