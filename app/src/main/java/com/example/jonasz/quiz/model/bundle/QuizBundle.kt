package com.example.jonasz.quiz.model.bundle

import java.io.Serializable

class QuizBundle(val id: Long,
                 val title: String,
                 var questionIndex: Int,
                 var numberOfCorrectAnswers: Int,
                 var finished: Boolean) : Serializable