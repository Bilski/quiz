package com.example.jonasz.quiz.mvpviews

import com.example.jonasz.quiz.model.bundle.QuizBundle
import io.appflate.droidmvp.DroidMVPView

interface ListingView : DroidMVPView {

    fun showQuizzes()

    fun showQuizDetailScreen(quiz: QuizBundle)

}