package com.example.jonasz.quiz.mvpviews

import io.appflate.droidmvp.DroidMVPView

interface QuizDetailView : DroidMVPView {

    fun showAnswers()

    fun showQuizTitle(text: String)

    fun showQuestion(text: String)

    fun updateProgress(progress: Int)

    fun setMaxNumberOnProgressBar(maxNumber: Int)

    fun setImage(url: String?)

    fun hideImage()

    fun showImage()

    fun showResultScreen(result: String)

    fun close()
}