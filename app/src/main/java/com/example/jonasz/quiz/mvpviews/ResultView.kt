package com.example.jonasz.quiz.mvpviews

import io.appflate.droidmvp.DroidMVPView

interface ResultView : DroidMVPView {

    fun showResult(result: String)
}