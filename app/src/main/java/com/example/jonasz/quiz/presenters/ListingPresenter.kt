package com.example.jonasz.quiz.presenters

import com.example.jonasz.quiz.ListingRowView
import com.example.jonasz.quiz.dataLayer.entity.Listing
import com.example.jonasz.quiz.dataLayer.entity.Quiz
import com.example.jonasz.quiz.dataLayer.rdp.AllQuizzesRetrofitSpecification
import com.example.jonasz.quiz.dataLayer.rdp.Repository
import com.example.jonasz.quiz.model.ListingPresentationModel
import com.example.jonasz.quiz.model.bundle.QuizBundle
import com.example.jonasz.quiz.mvpviews.ListingView
import io.appflate.droidmvp.SimpleDroidMVPPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class ListingPresenter(private val repository: Repository<Quiz>) : SimpleDroidMVPPresenter<ListingView, ListingPresentationModel>() {

    private lateinit var disposable: Disposable

    override fun attachView(mvpView: ListingView, presentationModel: ListingPresentationModel) {
        super.attachView(mvpView, presentationModel)
        if (presentationModel.shouldFetchQuizzes()) {
            fetchQuizzes()
        } else {
            mvpView.showQuizzes()
        }
    }

    fun getNumberOfQuizzes(): Int {
        return presentationModel.getNumberOfQuizzes()
    }

    fun onItemClick(position: Int) {
        val quiz = presentationModel.getQuiz(position)
        val quizBundle = QuizBundle(id = quiz.id,
            title = quiz.title,
            questionIndex = quiz.questionIndex,
            numberOfCorrectAnswers = quiz.numberOfCorrectAnswers,
            finished = false)

        mvpView.showQuizDetailScreen(quizBundle)
    }

    private fun fetchQuizzes() {
        disposable = repository
            .query(AllQuizzesRetrofitSpecification())
            .subscribeOn(Schedulers.io()) // TO-DO idealnie byloby wstrzykiwac scheduler w konstruktorze do unit testow
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { onQuizzesFetched(it) },
                onError = {
                    it.printStackTrace()
                }
            )
    }

    private fun onQuizzesFetched(quizzes: List<Quiz>) {
        presentationModel.setQuizzes(quizzes)
        mvpView.showQuizzes()
    }

    internal fun onBindQuizRowViewAtPosition(position: Int, rowView: ListingRowView) {
        val quiz = presentationModel.getQuiz(position)

        rowView.setTitle(quiz.title)
        rowView.setThumbnail(quiz.photo?.url)

        if (quiz.finished) {
            showResult(quiz, rowView)
        } else {
            showProgress(quiz, rowView)
        }
    }

    private fun showResult(quiz: Quiz, rowView: ListingRowView) {
        val numberOfCorrectAnswers = quiz.numberOfCorrectAnswers
        val numberOfAllQuestions = quiz.questions
        val result = numberOfCorrectAnswers.toFloat() / numberOfAllQuestions.toFloat() * 100
        rowView.setResult("Ostatni wynik: " + result.toString() + "%")
    }

    private fun showProgress(quiz: Quiz, rowView: ListingRowView) {
        if (quiz.questionIndex == 0) {
            rowView.hideResult()
            return
        }

        val progress = quiz.questionIndex.toFloat() / quiz.questions.toFloat() * 100
        rowView.setResult("Ukończyłeś quiz w " + progress.toString() + "%")
    }

    fun unbind() = disposable.dispose()
}