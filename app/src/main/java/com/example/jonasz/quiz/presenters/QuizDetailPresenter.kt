package com.example.jonasz.quiz.presenters

import com.example.jonasz.quiz.AnswerRowView
import com.example.jonasz.quiz.dataLayer.entity.QuizDetail
import com.example.jonasz.quiz.dataLayer.rdp.AllQuizDetailRetrofitSpecification
import com.example.jonasz.quiz.dataLayer.rdp.Repository
import com.example.jonasz.quiz.model.QuizDetailPresentationModel
import com.example.jonasz.quiz.mvpviews.QuizDetailView
import io.appflate.droidmvp.SimpleDroidMVPPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class QuizDetailPresenter(private val repository: Repository<QuizDetail>) : SimpleDroidMVPPresenter<QuizDetailView, QuizDetailPresentationModel>()  {

    private lateinit var disposable: Disposable

    override fun attachView(mvpView: QuizDetailView, presentationModel: QuizDetailPresentationModel) {
        super.attachView(mvpView, presentationModel)

        if (presentationModel.shouldFetchQuestions()) {
            fetchQuizDetails()
        } else {
            showQuestion()
        }

    }

    private fun fetchQuizDetails() {
        val specification = AllQuizDetailRetrofitSpecification()
        specification.quizId = presentationModel.getQuizId()

        disposable = repository.query(specification)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { onQuizDetailFetched(it.first()) },
                onError =  {
                    it.printStackTrace()
                }
            )
    }

    private fun onQuizDetailFetched(quizDetail: QuizDetail) {
        presentationModel.setQuestions(questions = quizDetail.questions)

        mvpView.setMaxNumberOnProgressBar(presentationModel.getNumberOfQuestions())
        mvpView.showQuizTitle(presentationModel.getQuizTitle())

        showQuestion()
    }

    fun onBindAnswerRowViewAtPosition(position: Int, rowView: AnswerRowView) {
        val currentAnswers = presentationModel.getCurrentAnswers()
        val currentAnswer = currentAnswers[position]

        rowView.setAnswer(currentAnswer.text)
    }

    fun onItemClick(position: Int) {
        saveResult(position)

        if (presentationModel.shouldGoToResultScreen()) {
            presentationModel.setFinishedFlagWithNewState(true)
            presentationModel.resetQuestionIndex()
            updateQuizInDB()
            goToResultScreen()
        } else {
            incrementCurrentQuestionIndex()
            mvpView.updateProgress(presentationModel.getCurrentQuestionIndex())
            showQuestion()
        }
    }

    fun onBackButtonClicked() {
        updateQuizInDB() //w completion handlerze zamkniemy mvpView.close()
        mvpView.close()
    }

    private fun updateQuizInDB() {
        //zapisujemy do bazy
    }

    private fun goToResultScreen() {
        val numberOfCorrectAnswers = presentationModel.getNumberOfCorrectAnswers()
        val numberOfAllQuestions = presentationModel.getNumberOfQuestions()
        val resultInPercentage = numberOfCorrectAnswers.toFloat() / numberOfAllQuestions.toFloat() * 100

        mvpView.showResultScreen(resultInPercentage.toString() + "%")
    }

    private fun showQuestion() {
        mvpView.showQuestion(presentationModel.getCurrentQuestionTitle())
        mvpView.showAnswers()

        val imageUrl = presentationModel.getQuestionPhotoUrl()

        if (imageUrl != null) {
            mvpView.showImage()
            mvpView.setImage(imageUrl)
        } else {
            mvpView.hideImage()
        }
    }

    private fun saveResult(position: Int) {
        presentationModel.incrementCorrectAnswerIfNeeded(position)

    }

    private fun incrementCurrentQuestionIndex() {
        presentationModel.incrementCurrentQuestionIndex()
    }

    fun unbind() = disposable.dispose()

    fun getNumberOfCurrentAnswers() =
        presentationModel.getCurrentAnswers()
        .size
}