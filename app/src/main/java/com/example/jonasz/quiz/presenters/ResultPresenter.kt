package com.example.jonasz.quiz.presenters

import com.example.jonasz.quiz.model.ResultPresentationModel
import com.example.jonasz.quiz.mvpviews.ResultView
import io.appflate.droidmvp.SimpleDroidMVPPresenter

class ResultPresenter : SimpleDroidMVPPresenter<ResultView, ResultPresentationModel>() {

    override fun attachView(mvpView: ResultView, presentationModel: ResultPresentationModel) {
        super.attachView(mvpView, presentationModel)

        mvpView.showResult(presentationModel.getResult())
    }
}