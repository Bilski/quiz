package com.example.jonasz.quiz.endpointProvider

import com.example.jonasz.quiz.dataLayer.endpoint.EndpointProvider
import org.junit.Assert.assertEquals
import org.junit.Test

class EndpointProviderTests {

    private val LISTING_ENDPOINT = "api/v1/quizzes/0/100"
    private val QUIZ_ENDPOINT = "api/v1/quiz/{id}/0"

    @Test
    fun endpointProvider_ReturnsCorrectListingEndpoint() {
        val result = EndpointProvider.getListingEndpoint()

        assertEquals(LISTING_ENDPOINT, result)
    }
    
    @Test
    fun endpointProvider_ReturnsCorrectQuizEndpoint() {
        val quizId: Long = 2
        val expectedQuizEndpoint = QUIZ_ENDPOINT.replace("{id}", quizId.toString())

        val result = EndpointProvider.getQuizEndpoint(quizId)

        assertEquals(expectedQuizEndpoint, result)
    }
}

