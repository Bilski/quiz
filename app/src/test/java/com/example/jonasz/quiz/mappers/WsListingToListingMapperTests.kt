//package com.example.jonasz.quiz.mappers
//
//import com.example.jonasz.quiz.dataLayer.entity.Listing
//import com.example.jonasz.quiz.dataLayer.entity.Photo
//import com.example.jonasz.quiz.dataLayer.entity.Quiz
//import com.example.jonasz.quiz.dataLayer.model.WsListing
//import com.example.jonasz.quiz.dataLayer.model.WsPhoto
//import com.example.jonasz.quiz.dataLayer.model.WsQuiz
//import junit.framework.Assert
//import org.junit.Test
//
//class WsListingToListingMapperTests {
//
//    private val mapper = WsListingToListingMapper()
//
//    @Test
//    fun wsListingToListingMapper_ReturnsCorrectlyMappedObject_WhenAllPropertiesExist() {
//        val listingToMap = WsListing(count = 2,
//            quizzes = listOf(
//                WsQuiz(
//                    content = "content",
//                    id = 1,
//                    photo = WsPhoto(author = "author",
//                        title = "",
//                        url = "http://example.com",
//                        source = "source"
//                    ),
//                    questions = 2,
//                    sponsored = true,
//                    title = "title",
//                    type = "type"
//                ),
//                WsQuiz(
//                    content = "content2",
//                    id = 2,
//                    photo = WsPhoto(author = "author2",
//                        title = "",
//                        url = "http://example2.com",
//                        source = "source2"
//                    ),
//                    questions = 2,
//                    sponsored = true,
//                    title = "title2",
//                    type = "type"
//                )
//            )
//        )
//
//        val result = mapper.mapOrNull(listingToMap)!!
//
//        assertThatTwoListingsAreEqual(listingToMap, result)
//    }
//
//    @Test
//    fun wsListingToListingMapper_ReturnsNull_WhenCountIsNull() {
//        val listingToMap = WsListing(count = null,
//            quizzes = listOf(
//                WsQuiz(
//                    content = "content",
//                    id = 1,
//                    photo = WsPhoto(author = "author",
//                        title = "",
//                        url = "http://example.com",
//                        source = "source"
//                    ),
//                    questions = 2,
//                    sponsored = true,
//                    title = "title",
//                    type = "type"
//                ),
//                WsQuiz(
//                    content = "content2",
//                    id = 2,
//                    photo = WsPhoto(author = "author2",
//                        title = "",
//                        url = "http://example2.com",
//                        source = "source2"
//                    ),
//                    questions = 2,
//                    sponsored = true,
//                    title = "title2",
//                    type = "type"
//                )
//            )
//        )
//
//        val result = mapper.mapOrNull(listingToMap)
//
//        Assert.assertNull(result)
//    }
//
//    @Test
//    fun wsListingToListingMapper_ReturnsNull_WhenCountIsZero() {
//        val listingToMap = WsListing(count = 0,
//            quizzes = listOf(
//                WsQuiz(
//                    content = "content",
//                    id = 1,
//                    photo = WsPhoto(author = "author",
//                        title = "",
//                        url = "http://example.com",
//                        source = "source"
//                    ),
//                    questions = 2,
//                    sponsored = true,
//                    title = "title",
//                    type = "type"
//                ),
//                WsQuiz(
//                    content = "content2",
//                    id = 2,
//                    photo = WsPhoto(author = "author2",
//                        title = "",
//                        url = "http://example2.com",
//                        source = "source2"
//                    ),
//                    questions = 2,
//                    sponsored = true,
//                    title = "title2",
//                    type = "type"
//                )
//            )
//        )
//
//        val result = mapper.mapOrNull(listingToMap)
//
//        Assert.assertNull(result)
//    }
//
//    @Test
//    fun wsListingToListingMapper_ReturnsNull_WhenQuizzesAreNull() {
//        val listingToMap = WsListing(count = 0,
//            quizzes = null)
//
//        val result = mapper.mapOrNull(listingToMap)
//
//        Assert.assertNull(result)
//    }
//
//    @Test
//    fun wsListingToListingMapper_ReturnsObjectWithQuizzesEqualToEmptyList_WhenQuizzesListIsEmpty() {
//        val listingToMap = WsListing(count = 2,
//            quizzes = emptyList()
//        )
//
//        val result = mapper.mapOrNull(listingToMap)!!
//
//        Assert.assertEquals(result.quizzes.size, 0)
//    }
//
//    private fun assertThatTwoPhotosAreEqual(wsPhoto: WsPhoto, photo: Photo) {
//        Assert.assertEquals(wsPhoto.author, photo.author)
//        Assert.assertEquals(wsPhoto.source, photo.source)
//        Assert.assertEquals(wsPhoto.title, photo.title)
//        Assert.assertEquals(wsPhoto.url, photo.url)
//    }
//
//    private fun assertThatTwoQuizzesAreEqual(wsQuiz: WsQuiz, quiz: Quiz) {
//        Assert.assertEquals(wsQuiz.content, quiz.content)
//        Assert.assertEquals(wsQuiz.id, quiz.id)
//        Assert.assertEquals(wsQuiz.questions, quiz.questions)
//        Assert.assertEquals(wsQuiz.sponsored, quiz.sponsored)
//        Assert.assertEquals(wsQuiz.title, quiz.title)
//        Assert.assertEquals(wsQuiz.type, quiz.type)
//        assertThatTwoPhotosAreEqual(wsQuiz.photo!!, quiz.photo!!)
//    }
//
//    private fun assertThatTwoListingsAreEqual(wsListing: WsListing, listing: Listing) {
//        Assert.assertEquals(wsListing.count, listing.count)
//
//        wsListing.quizzes!!.forEachIndexed { index, wsQuiz ->
//            assertThatTwoQuizzesAreEqual(wsQuiz!!, listing.quizzes[index])
//        }
//    }
//}