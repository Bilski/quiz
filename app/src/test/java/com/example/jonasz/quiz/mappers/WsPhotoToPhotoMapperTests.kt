package com.example.jonasz.quiz.mappers

import com.example.jonasz.quiz.dataLayer.entity.Photo
import com.example.jonasz.quiz.dataLayer.mappers.WsPhotoToPhotoMapper
import com.example.jonasz.quiz.dataLayer.model.WsPhoto
import junit.framework.Assert
import org.junit.Test

class WsPhotoToPhotoMapperTests {

    private val mapper = WsPhotoToPhotoMapper()

    @Test
    fun wsPhotoToPhotoMapper_ReturnsCorrectlyMappedObject_WhenAllPropertiesExist() {
        val photoToMap = WsPhoto(
            author = "author",
            source = "source",
            title = "title",
            url = "http://example.com"
        )

        val result = mapper.mapOrNull(photoToMap)!!

        assertThatTwoPhotosAreEqual(photoToMap, result)
    }

    @Test
    fun wsPhotoToPhotoMapper_ReturnsNull_WhenUrlIsNull() {
        val photoToMap = WsPhoto(
            author = "author",
            source = "source",
            title = "title",
            url = null
        )

        val result = mapper.mapOrNull(photoToMap)

        Assert.assertNull(result)
    }

    @Test
    fun wsPhotoToPhotoMapper_ReturnsNull_WhenUrlIsBlank() {
        val photoToMap = WsPhoto(
            author = "author",
            source = "source",
            title = "title",
            url = ""
        )

        val result = mapper.mapOrNull(photoToMap)

        Assert.assertNull(result)
    }


    @Test
    fun wsPhotoToPhotoMapper_ReturnsObjectWithAuthorEqualToEmptyString_WhenAuthorIsNull() {
        val photoToMap = WsPhoto(
            author = null,
            source = "source",
            title = "title",
            url = "http://example.com"
        )

        val result = mapper.mapOrNull(photoToMap)!!

        Assert.assertEquals("", result.author)
    }

    @Test
    fun wsPhotoToPhotoMapper_ReturnsObjectWithSourceEqualToEmptyString_WhenSourceIsNull() {
        val photoToMap = WsPhoto(
            author = "author",
            source = null,
            title = "title",
            url = "http://example.com"
        )

        val result = mapper.mapOrNull(photoToMap)!!

        Assert.assertEquals("", result.source)
    }

    @Test
    fun wsPhotoToPhotoMapper_ReturnsObjectWithTitleEqualToEmptyString_WhenTitleIsNull() {
        val photoToMap = WsPhoto(
            author = "author",
            source = "source",
            title = null,
            url = "http://example.com"
        )

        val result = mapper.mapOrNull(photoToMap)!!

        Assert.assertEquals("", result.title)
    }

    private fun assertThatTwoPhotosAreEqual(wsPhoto: WsPhoto, photo: Photo) {
        Assert.assertEquals(wsPhoto.author, photo.author)
        Assert.assertEquals(wsPhoto.source, photo.source)
        Assert.assertEquals(wsPhoto.title, photo.title)
        Assert.assertEquals(wsPhoto.url, photo.url)
    }
}