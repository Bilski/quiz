package com.example.jonasz.quiz.mappers

import com.example.jonasz.quiz.dataLayer.entity.Photo
import com.example.jonasz.quiz.dataLayer.entity.Quiz
import com.example.jonasz.quiz.dataLayer.mappers.WsQuizToQuizMapper
import com.example.jonasz.quiz.dataLayer.model.WsPhoto
import com.example.jonasz.quiz.dataLayer.model.WsQuiz
import junit.framework.Assert
import org.junit.Test

class WsQuizToQuizMapperTests {

    private val mapper = WsQuizToQuizMapper()

    @Test
    fun wsQuizToQuizMapper_ReturnsCorrectlyMappedObject_WhenAllPropertiesExist() {
        val quizToMap = WsQuiz(
            content = "content",
            id = 1,
            photo = WsPhoto(author = "author",
                title = "",
                url = "http://example.com",
                source = "source"
            ),
            questions = 2,
            sponsored = true,
            title = "title",
            type = "type"
        )

        val result = mapper.mapOrNull(quizToMap)!!

        assertThatTwoQuizzesAreEqual(quizToMap, result)
    }

    @Test
    fun wsQuizToQuizMapper_ReturnsEmptyString_WhenContentIsNull() {
        val quizToMap = WsQuiz(
            content = null,
            id = 1,
            photo = WsPhoto(author = "author",
                title = "",
                url = "http://example.com",
                source = "source"
            ),
            questions = 2,
            sponsored = true,
            title = "title",
            type = "type"
        )

        val result = mapper.mapOrNull(quizToMap)!!

        Assert.assertEquals("", result.content)
    }

    @Test
    fun wsQuizToQuizMapper_ReturnsNull_WhenIdIsNull() {
        val quizToMap = WsQuiz(
            content = "content",
            id = null,
            photo = WsPhoto(author = "author",
                title = "",
                url = "http://example.com",
                source = "source"
            ),
            questions = 2,
            sponsored = true,
            title = "title",
            type = "type"
        )

        val result = mapper.mapOrNull(quizToMap)

        Assert.assertNull(result)
    }

    // mapper do photo powinien zostac zmockowany i powinnismy sprawdzac
    // czy zastubowana wartosc jest ustawiana (1) oraz metoda na mocku wolana (2)

    @Test
    fun wsQuizToQuizMapper_ReturnsObjectWithPhotoEqualToNull_WhenPhotoIsNull() {
        val quizToMap = WsQuiz(
            content = "content",
            id = 1,
            photo = null,
            questions = 2,
            sponsored = true,
            title = "title",
            type = "type"
        )

        val result = mapper.mapOrNull(quizToMap)!!

        Assert.assertNull(result.photo)
    }

    @Test
    fun wsQuizToQuizMapper_ReturnsNull_WhenQuestionsIsNull() {
        val quizToMap = WsQuiz(
            content = "content",
            id = 1,
            photo = WsPhoto(author = "author",
                title = "",
                url = "http://example.com",
                source = "source"
            ),
            questions = null,
            sponsored = true,
            title = "title",
            type = "type"
        )

        val result = mapper.mapOrNull(quizToMap)

        Assert.assertNull(result)
    }

    @Test
    fun wsQuizToQuizMapper_ReturnsObjectWithSponsoredEqualToFalse_WhenSponsoredIsNull() {
        val quizToMap = WsQuiz(
            content = "content",
            id = 1,
            photo = WsPhoto(author = "author",
                title = "",
                url = "http://example.com",
                source = "source"
            ),
            questions = 2,
            sponsored = null,
            title = "title",
            type = "type"
        )

        val result = mapper.mapOrNull(quizToMap)!!

        Assert.assertFalse(result.sponsored)
    }

    @Test
    fun wsQuizToQuizMapper_ReturnsNull_WhenTitleIsNull() {
        val quizToMap = WsQuiz(
            content = "content",
            id = 1,
            photo = WsPhoto(author = "author",
                title = "",
                url = "http://example.com",
                source = "source"
            ),
            questions = 2,
            sponsored = true,
            title = null,
            type = "type"
        )

        val result = mapper.mapOrNull(quizToMap)

        Assert.assertNull(result)
    }

    @Test
    fun wsQuizToQuizMapper_ReturnsNull_WhenTitleIsBlank() {
        val quizToMap = WsQuiz(
            content = "content",
            id = null,
            photo = WsPhoto(author = "author",
                title = "",
                url = "http://example.com",
                source = "source"
            ),
            questions = 2,
            sponsored = true,
            title = "",
            type = "type"
        )

        val result = mapper.mapOrNull(quizToMap)

        Assert.assertNull(result)
    }

    @Test
    fun wsQuizToQuizMapper_ReturnsObjectWithTypeEqualToEmptyString_WhenTypeIsNull() {
        val quizToMap = WsQuiz(
            content = "content",
            id = 1,
            photo = WsPhoto(author = "author",
                title = "",
                url = "http://example.com",
                source = "source"
            ),
            questions = 2,
            sponsored = true,
            title = "title",
            type = null
        )

        val result = mapper.mapOrNull(quizToMap)!!

        Assert.assertEquals("", result.type)
    }

    private fun assertThatTwoPhotosAreEqual(wsPhoto: WsPhoto, photo: Photo) {
        Assert.assertEquals(wsPhoto.author, photo.author)
        Assert.assertEquals(wsPhoto.source, photo.source)
        Assert.assertEquals(wsPhoto.title, photo.title)
        Assert.assertEquals(wsPhoto.url, photo.url)
    }

    private fun assertThatTwoQuizzesAreEqual(wsQuiz: WsQuiz, quiz: Quiz) {
        Assert.assertEquals(wsQuiz.content, quiz.content)
        Assert.assertEquals(wsQuiz.id, quiz.id)
        Assert.assertEquals(wsQuiz.questions, quiz.questions)
        Assert.assertEquals(wsQuiz.sponsored, quiz.sponsored)
        Assert.assertEquals(wsQuiz.title, quiz.title)
        Assert.assertEquals(wsQuiz.type, quiz.type)
        assertThatTwoPhotosAreEqual(wsQuiz.photo!!, quiz.photo!!)
    }
}